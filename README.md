# third-party-config

This projects contains the Third Party CI configuration:

- [pipelines](./zuul.d/pipelines.yaml): define triggers and reporters.
- [project config](./zuul.d/projects.yaml): assign jobs to the rpms projects.
